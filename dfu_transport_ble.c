#include <assert.h>
#include <glib.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <errno.h>

#include "dfu_transport_ble.h"
#include "crc32.h"

/*
Software Development Kit > nRF5 SDK v16.0.0 > Libraries > BLE Services > Buttonless Secure DFU Service
Software Development Kit > nRF5 SDK v16.0.0 > Libraries > Bootloader and DFU modules

The DFU BLE service uses three UUIDs.  UUID #3 (Buttonless) which was added to SDK12 and later
allows an application to put the bootloader into DFU MODE as opposed to a button on the board.
This is done by writing the 0x01 op to UUID #3 and acknowledging (Indications must be enabled).
The device will then disconnect from the client and enter bootloader DFU MODE.  It then starts rebroadcasting
on address + 1.  The client must reconnect on this address and then use UUIDs #1 and #2 writing the 
protocol operations listed in the nordic documentation. 

Not all DFU operations are supported in older SDK versions.  SDK 14 and earlier only have the first 6 operations.

Note: sizeof() returns n bytes, so an array of ints is not the same as an array of chars.
array size = sizeof(arr) / sizeof(arr[0])
int* ptr = (int*)malloc(10 * sizeof(int)); 

*/

#define TEST_ADDR	"FC:E6:1A:75:AA:5D"
//#define TEST_ADDR	"DE:62:86:D5:0F:F7"

#define DFU_CONTROL_CHARACTERISTIC_UUID 	"8ec90001-f315-4f60-9fb8-838830daea50"
#define DFU_DATA_CHARACTERISTIC_UUID 		"8ec90002-f315-4f60-9fb8-838830daea50"
#define DFU_BUTTONLESS_CHARACTERISTIC_UUID 	"8ec90003-f315-4f60-9fb8-838830daea50"


#define BLE_DATA_SIZE_MAX		1024
#define MIN(a,b) (((a) < (b)) ? (a) : (b))

uuid_t m_dfu_ctl_uuid;
uuid_t m_dfu_data_uuid;
uuid_t m_dfu_btnls_uuid;

static uint16_t m_prn = 0;
static uint16_t m_mtu = 0;

// TODO: maybe combine these into a struct
static uint8_t g_receive_data[BLE_DATA_SIZE_MAX];
static size_t g_receive_data_len;

static GMainLoop *g_main_loop;

static void Ctl_UUID_NotificationHandler(const uuid_t* uuid, const uint8_t* data, size_t data_length, void* user_data);

// Little Endian funcs
static uint16_t get_uint16_le(const uint8_t *p_data)
{
	uint16_t data;

	data  = ((uint16_t)*(p_data + 0) << 0);
	data += ((uint16_t)*(p_data + 1) << 8);

	return data;
}

static void put_uint16_le(uint8_t *p_data, uint16_t data)
{
	*(p_data + 0) = (uint8_t)(data >> 0);
	*(p_data + 1) = (uint8_t)(data >> 8);
}

static uint32_t get_uint32_le(const uint8_t *p_data)
{
	uint32_t data;

	data  = ((uint32_t)*(p_data + 0) <<  0);
	data += ((uint32_t)*(p_data + 1) <<  8);
	data += ((uint32_t)*(p_data + 2) << 16);
	data += ((uint32_t)*(p_data + 3) << 24);

	return data;
}

static void put_uint32_le(uint8_t *p_data, uint32_t data)
{
	*(p_data + 0) = (uint8_t)(data >>  0);
	*(p_data + 1) = (uint8_t)(data >>  8);
	*(p_data + 2) = (uint8_t)(data >> 16);
	*(p_data + 3) = (uint8_t)(data >> 24);
}

int BLE_InitUUIDs(void)
{
	int ret_code;
	
	ret_code = gattlib_string_to_uuid(DFU_CONTROL_CHARACTERISTIC_UUID, strlen(DFU_CONTROL_CHARACTERISTIC_UUID), &m_dfu_ctl_uuid);
	if(ret_code)
	{
		//TODO
	}
	gattlib_string_to_uuid(DFU_DATA_CHARACTERISTIC_UUID, strlen(DFU_DATA_CHARACTERISTIC_UUID), &m_dfu_data_uuid);
	gattlib_string_to_uuid(DFU_BUTTONLESS_CHARACTERISTIC_UUID, strlen(DFU_BUTTONLESS_CHARACTERISTIC_UUID), &m_dfu_btnls_uuid);
	return GATTLIB_SUCCESS;
}

/**---------------------------------------------------------------------------
 * @brief	Connects BLE to the target address and fills the sensorID_t connection handle.
 *			Will run a scan if connection fails...
 *			If a BLE device has not been discovered in a scan (not in some HCI adapter list),
 *			the gattlib_connect() func will fail with the following example:
 *				Device 'DD:EB:AE:90:BC:CE' cannot be found
 *				Failed to connect to the bluetooth device.
 *
 * @param	p_sm = pointer to the sensor id data struct.
 *
 * @return	gattlib return codes
 *---------------------------------------------------------------------------*/
int dfu_ble_connect(sensorID_t *p_sm)
{
	int ret_code;
	
	// TODO: this can be done once at process startup.
	ret_code = BLE_InitUUIDs();
	if(ret_code != GATTLIB_SUCCESS)
	{
		printf("Bad UUID\r\n");
		return ret_code;
	}

	p_sm->p_connection = gattlib_connect(NULL, p_sm->p_addr, GATTLIB_CONNECTION_OPTIONS_LEGACY_DEFAULT);
	if(p_sm->p_connection == NULL) 
	{
		fprintf(stderr, "Failed to connect. Scanning...\n");
		// TODO: add scanning
		return GATTLIB_NOT_FOUND;
	}
	printf("CONNECTED\r\n");
	
	// TODO: might have to register and start in each operation func.
	// However the data UUID#2 doesnt send a response so why register and start notifications on it?
	gattlib_register_notification(p_sm->p_connection, Ctl_UUID_NotificationHandler, NULL);

	ret_code = gattlib_notification_start(p_sm->p_connection, &m_dfu_ctl_uuid);
	if (ret_code != GATTLIB_SUCCESS)
	{
		fprintf(stderr, "Fail to start notification.\n");
		return ret_code;
	}
	
	// create block mechanism. TODO: research this
	g_main_loop = g_main_loop_new(NULL, 0);

	return GATTLIB_SUCCESS;
}

/**---------------------------------------------------------------------------
 * @brief	Disconnects BLE host from the target address.
 *
 * @param	p_sm = pointer to the sensor id data struct.
 *
 * @return	gattlib return codes
 *---------------------------------------------------------------------------*/
int dfu_ble_disconnect(sensorID_t *p_sm)
{
	int ret_code;
	
	ret_code = gattlib_notification_stop(p_sm->p_connection, &m_dfu_ctl_uuid);
	ret_code = gattlib_disconnect(p_sm->p_connection);
	
	return ret_code;
}

/**---------------------------------------------------------------------------
 * @brief	Sends control data to DFU contol point UUID
 *
 * @param	p_sm = pointer to the sensor id data struct.
 * @param	p_Data = pointer to data array.
 * @param	data_length = length of data array.
 *
 * @return	gattlib return codes
 *---------------------------------------------------------------------------*/
static int dfu_write_control_uuid(sensorID_t *p_sm, uint8_t *p_Data, size_t data_length)
{
	int ret_code;

	ret_code = gattlib_write_char_by_uuid(p_sm->p_connection, &m_dfu_ctl_uuid, p_Data, data_length);
	if(ret_code != GATTLIB_SUCCESS)
	{
		return ret_code;
	}

	/*
	// timeout setup
	timerclear(&g_tval.it_interval);	// zero interval means no reset of timer
	timerclear(&g_tval.it_value);
	g_tval.it_value.tv_sec = 10;	// second timeout 
	signal(SIGALRM, TimeoutHandler);
	setitimer(ITIMER_REAL, &g_tval, NULL);	// start
	*/

	// Block here to allow notification handler to be called.
	// Released in the Ctl_UUID_NotificationHandler()
	g_main_loop_run(g_main_loop); 

	return ret_code;
}

/**---------------------------------------------------------------------------
 * @brief	Receives asyncronous data from DFU Control Point UUID.
 *			All operation responses hit this one time (fit in a single BLE packet)
 *			so the main_loop is cleared to return control to the calling func.
 *
 * @param	uuid = The characteristic for which this is called.
 * @param	data = pointer to the response data from the chracteristic.
 * @param	data_length = length of response data array.
 * @param	user_data = passed to this function.
 *
 * @return	gattlib return codes
 *---------------------------------------------------------------------------*/
static void Ctl_UUID_NotificationHandler(const uuid_t* uuid, const uint8_t* data, size_t data_length, void* user_data) 
{	
	//nrf_dfu_op_t operation = (nrf_dfu_op_t) user_data;
	//g_receive_data = data;
	g_receive_data_len = data_length;	
	printf("NotificationHandler(): len = %d\r\n", data_length);
	
	memset(g_receive_data, 0, sizeof(g_receive_data));
	for(int i = 0; i < data_length; i++) 
	{
		g_receive_data[i] = data[i];
		printf("<%02X>", data[i]);
	}
	//printf("Done\r\n");
	
	
	// Unblock calling func
	g_main_loop_quit(g_main_loop);
}

/**---------------------------------------------------------------------------
 * @brief	Examines the response data per the operation sent.
 *			Each response should be at least three characters:
 *			<0> = NRF_DFU_OP_RESPONSE
 *			<1> = operation sent
 *			<2> = Nordic return code
 *
 *	NOTE: The gattlib funcs and DFU lib funcs use different return codes respectivly.
 *	The return code for "Operation successful" (0x01) for the Nordic DFU library on the target
 *	is different from the GATTLIB_SUCCESS (0x00) code for the gattlib BLUEZ wrapper library.
 *
 * @param	operation = DFU operation sent
 *
 * @return	Nordic DFU return codes
 *---------------------------------------------------------------------------*/
static int dfu_ble_test_rsp(nrf_dfu_op_t operation)
{
	int ret_code;
	
	if ((g_receive_data_len >= 3) && (g_receive_data[0] == NRF_DFU_OP_RESPONSE) && (g_receive_data[1] == operation))
	{
		if (g_receive_data[2] != NRF_DFU_RES_CODE_SUCCESS)
		{
			uint16_t rsp_error = g_receive_data[2];

			// get 2-byte error code, if applicable
			if (g_receive_data_len >= 4)
				rsp_error = (rsp_error << 8) + g_receive_data[3];

			//TODO: map result code to string name
			printf("Bad result code (0x%X)!\r\n", rsp_error);
			ret_code = rsp_error;
		}
		else
			ret_code = NRF_DFU_RES_CODE_SUCCESS;
	}
	else
	{
		printf("Invalid response!");
		ret_code = NRF_DFU_RES_CODE_OPERATION_FAILED;
	}
	return ret_code;
}

/**---------------------------------------------------------------------------
 * @brief	Triggers the bootloader to create an object of the specified type.
 *
 * @param	p_sm => Pointer to the sensor id data struct.
 * @param	obj_type => 1 = command (init) packet, 2 = data packet
 * @param	obj_size => object size in bytes.
 *
 * @return	Nordic DFU return codes
 *---------------------------------------------------------------------------*/
static int dfu_ble_create_obj(sensorID_t *p_sm, uint8_t obj_type, uint32_t obj_size)
{
	int ret_code;
	uint8_t send_data[6];

	send_data[0] = NRF_DFU_OP_OBJECT_CREATE;
	send_data[1] = obj_type;
	put_uint32_le(send_data + 2, obj_size);
	
	ret_code = dfu_write_control_uuid(p_sm, send_data, sizeof(send_data));
	if(ret_code != GATTLIB_SUCCESS)
	{
		printf("Gattlib Error: %d\r\n", ret_code);
		return NRF_DFU_RES_CODE_OPERATION_FAILED;
	}

	ret_code = dfu_ble_test_rsp(NRF_DFU_OP_OBJECT_CREATE);

	return ret_code;
}

/**---------------------------------------------------------------------------
 * Not supported this DFU version
 *---------------------------------------------------------------------------*/
static int dfu_ble_ping(sensorID_t *p_sm, uint8_t id)
{
	int ret_code;
	uint8_t send_data[2];

	send_data[0] = NRF_DFU_OP_PING;
	send_data[1] = id;

	printf("dfu_ble_ping()\r\n");
	ret_code = dfu_write_control_uuid(p_sm, send_data, 2);
	
	if(ret_code == GATTLIB_SUCCESS)
	{
		uint32_t resp_len;
		ret_code = dfu_ble_test_rsp(NRF_DFU_OP_PING);

		if(ret_code == GATTLIB_SUCCESS)
		{
			if (g_receive_data_len != 4 || g_receive_data[3] != id)
			{
				printf("Bad ping id!\r\n");
				ret_code = GATTLIB_DEVICE_ERROR;
			}
		}
	}

	return ret_code;
}

/**---------------------------------------------------------------------------
 * Not supported this DFU version
 *---------------------------------------------------------------------------*/
static int dfu_ble_get_fw_ver(sensorID_t *p_sm, uint8_t img_num)
{
	int ret_code;
	uint8_t send_data[2];

	send_data[0] = NRF_DFU_OP_FIRMWARE_VERSION;
	send_data[1] = img_num;

	printf("dfu_ble_get_fw_ver()\r\n");
	
	ret_code = dfu_write_control_uuid(p_sm, send_data, 2);
	if(ret_code != GATTLIB_SUCCESS)
	{
		printf("Gattlib Error: %d\r\n", ret_code);
		return ret_code;
	}
	
	ret_code = dfu_ble_test_rsp(NRF_DFU_OP_FIRMWARE_VERSION);

	return ret_code;
}

/**---------------------------------------------------------------------------
 * dfu_ble_set_prn()
 * 
 * @brief	Set Packet Receipt Notification
 *			Sets the number of packets to be sent before receiving a Packet 
 *			Receipt Notification. The default is 0.
 *
 * @param	p_sm => Pointer to the sensor id data struct.
 * @param	prn => number of packets to get a notification.
 *
 * @return	Nordic DFU return codes
 *---------------------------------------------------------------------------*/
static int dfu_ble_set_prn(sensorID_t *p_sm, uint16_t prn)
{
	int ret_code;
	uint8_t send_data[3];

	send_data[0]= NRF_DFU_OP_RECEIPT_NOTIF_SET;
	
	printf("dfu_ble_set_prn(%u)\r\n", prn);

	put_uint16_le(send_data + 1, prn);
	ret_code = dfu_write_control_uuid(p_sm, send_data, 3);

	if(ret_code == GATTLIB_SUCCESS)
	{
		ret_code = dfu_ble_test_rsp(NRF_DFU_OP_RECEIPT_NOTIF_SET);
	}
	else
		ret_code = NRF_DFU_RES_CODE_OPERATION_FAILED;

	return ret_code;
}

// Not supported
static int dfu_ble_get_mtu(sensorID_t *p_sm, uint16_t *p_mtu)
{
	int ret_code;
	uint8_t send_data[1];
	
	send_data[0] = NRF_DFU_OP_MTU_GET;

	printf("dfu_ble_get_mtu()\r\n");
	ret_code = dfu_write_control_uuid(p_sm, send_data, 1);

	if(ret_code == GATTLIB_SUCCESS)
	{
		uint32_t resp_len;

		ret_code = dfu_ble_test_rsp(NRF_DFU_OP_MTU_GET);

		if(ret_code == NRF_DFU_RES_CODE_SUCCESS)
		{
			if (g_receive_data_len == 5)
			{
				uint16_t mtu = get_uint16_le(g_receive_data + 3);

				*p_mtu = mtu;
			}
			else
			{
				printf("Invalid MTU!");
				ret_code = NRF_DFU_RES_CODE_OPERATION_FAILED;
			}
		}
	}

	return ret_code;
}

static int dfu_ble_get_crc(sensorID_t *p_sm, nrf_dfu_response_crc_t *p_crc_rsp)
{
	int ret_code;
	uint8_t send_data[1];
	send_data[0] = NRF_DFU_OP_CRC_GET;

	ret_code = dfu_write_control_uuid(p_sm, send_data, 1);
	if(ret_code != GATTLIB_SUCCESS)
	{
		printf("Gattlib Error: %d\r\n", ret_code);
		return ret_code;
	}
	
	ret_code = dfu_ble_test_rsp(NRF_DFU_OP_CRC_GET);
	if(ret_code == NRF_DFU_RES_CODE_SUCCESS)
	{
		if (g_receive_data_len == 11)
		{
			p_crc_rsp->offset = get_uint32_le(g_receive_data + 3);
			p_crc_rsp->crc    = get_uint32_le(g_receive_data + 7);

		}
		else
		{
			printf("Invalid CRC response!");
		}
	}
	return ret_code;
}

/**---------------------------------------------------------------------------
 * dfu_ble_select_obj()
 *
 * @brief	Sends the "select object" operation for all uuids(command and firmware).
 *			Checks for a connection handle. 
 *
 * @param	p_sm = pointer to the sensor id data struct, includes active connection handle.
 * @param	obj_type = enum nrf_dfu_obj_type_t.
 * @param	p_select_rsp = struct type of response specific to a select operation.
 *
 * @return	Nordic DFU return codes
 *---------------------------------------------------------------------------*/
static int dfu_ble_select_obj(sensorID_t *p_sm, uint8_t obj_type, nrf_dfu_response_select_t *p_select_rsp)
{
	int ret_code;
	uuid_t dfu_uuid;
	uint8_t send_data[3];
	
	send_data[0] = NRF_DFU_OP_OBJECT_SELECT;
	send_data[1] = obj_type;
	send_data[2] = 0;
	
	printf("dfu_ble_select_obj(type=%u)\r\n", obj_type);
	
	ret_code = dfu_write_control_uuid(p_sm, send_data, 2);
	if(ret_code != GATTLIB_SUCCESS)
	{
		printf("Gattlib Error: %d\r\n", ret_code);
		return ret_code;
	}
		
	ret_code = dfu_ble_test_rsp(NRF_DFU_OP_OBJECT_SELECT);
	if(ret_code == NRF_DFU_RES_CODE_SUCCESS)
	{
		if (g_receive_data_len == 15)
		{
			p_select_rsp->max_size = get_uint32_le(g_receive_data + 3);
			p_select_rsp->offset   = get_uint32_le(g_receive_data + 7);
			p_select_rsp->crc      = get_uint32_le(g_receive_data + 11);

			printf("\r\nObject selected:  max_size:%u offset:%u crc:0x%08X\r\n", p_select_rsp->max_size, p_select_rsp->offset, p_select_rsp->crc);
		}
		else
		{
			printf("Invalid object response!");
		}
	}
	return ret_code;
}

/**---------------------------------------------------------------------------
 * Not supported this DFU version
 *---------------------------------------------------------------------------*/
static int dfu_ble_test_op(sensorID_t *p_sm)
{
	int ret_code;
	uint8_t send_data[2];

	send_data[0] = NRF_DFU_OP_OBJECT_WRITE;
	send_data[1] = 7;
	
	ret_code = dfu_write_control_uuid(p_sm, send_data, 2);
	if(ret_code != GATTLIB_SUCCESS)
	{
		printf("Gattlib Error: %d\r\n", ret_code);
		return NRF_DFU_RES_CODE_OPERATION_FAILED;
	}

	ret_code = dfu_ble_test_rsp(NRF_DFU_OP_OBJECT_WRITE);

	return ret_code;
}

static int dfu_ble_stream_data(sensorID_t *p_sm, const uint8_t *p_data, uint32_t data_size)
{
	int ret_code = 0;
	uint32_t pos, stp, stp_max;
	uint8_t send_data[data_size];
	uint16_t mtu = 20;

	if (p_data == NULL || data_size == 0)
	{
		ret_code = 1;
	}
/* ????????????
	if (!ret_code)
	{
		if (mtu >= 5)
		{
			stp_max = (mtu - 1) / 2 - 1;
		}
		else
		{
			printf("MTU is too small to send data!\r\n");
			ret_code = 1;
		}
	}
*/
/*
	for (pos = 0; !err_code && pos < data_size; pos += stp)
	{
		send_data[0] = NRF_DFU_OP_OBJECT_WRITE;	// not supported
		stp = MIN((data_size - pos), stp_max);
		memcpy(send_data + 1, p_data + pos, stp);
		//err_code = dfu_serial_send(p_uart, send_data, stp + 1);
	}
*/
	for(pos = 0; pos < data_size; pos += stp)
	{
		stp = MIN((data_size - pos), mtu);	// probably to handle last chunk that may be smaller than stp_max
		memcpy(send_data, p_data + pos, stp);
		for(int i = 0; i < stp; i++) 
		{
			printf("<%02X>", send_data[i]);
		}
		//ret_code = gattlib_write_char_by_uuid(p_sm->p_connection, &m_dfu_data_uuid, send_data, stp);
		if(ret_code)
			break;
	}

	return ret_code;
}


/**
 *
 */
static int dfu_ble_stream_data_crc(sensorID_t *p_sm, const uint8_t *p_data, uint32_t data_size, uint32_t pos, uint32_t *p_crc)
{
	int ret_code;
	nrf_dfu_response_crc_t rsp_crc;

	printf("dfu_ble_stream_data_crc(): len=%u offset=%u crc=0x%08X", data_size, pos, *p_crc);

	ret_code = dfu_ble_stream_data(p_sm, p_data, data_size);

	if (!ret_code)
	{
		*p_crc = crc32_compute(p_data, data_size, p_crc);

		ret_code = dfu_ble_get_crc(p_sm, &rsp_crc);
	}

	if (!ret_code)
	{
		if (rsp_crc.offset != pos + data_size)
		{
			printf("Invalid offset (%u -> %u)!", pos + data_size, rsp_crc.offset);

			ret_code = 2;
		}
		if (rsp_crc.crc != *p_crc)
		{
			printf("Invalid CRC (0x%08X -> 0x%08X)!", *p_crc, rsp_crc.crc);

			ret_code = 2;
		}
	}

	return ret_code;
}
/*
static int dfu_serial_try_to_recover_ip(sensorID_t *p_sm, const uint8_t *p_data, uint32_t data_size,
										nrf_dfu_response_select_t *p_rsp_recover,
										const nrf_dfu_response_select_t *p_rsp_select)
{
	int ret_code;
	uint32_t pos_start, len_remain;
	uint32_t crc_32;

	*p_rsp_recover = *p_rsp_select;
	pos_start = p_rsp_recover->offset;

	if (pos_start > 0 && pos_start <= data_size)
	{
		crc_32 = crc32_compute(p_data, pos_start, NULL);

		if (p_rsp_select->crc != crc_32)
		{
			pos_start = 0;
		}
	}
	else
	{
		pos_start = 0;
	}
											
}
/*
static int dfu_serial_try_to_recover_ip(uart_drv_t *p_uart, const uint8_t *p_data, uint32_t data_size,
										nrf_dfu_response_select_t *p_rsp_recover,
										const nrf_dfu_response_select_t *p_rsp_select)
{
	int err_code = 0;
	uint32_t pos_start, len_remain;
	uint32_t crc_32;

	*p_rsp_recover = *p_rsp_select;

	pos_start = p_rsp_recover->offset;

	if (pos_start > 0 && pos_start <= data_size)
	{
		crc_32 = crc32_compute(p_data, pos_start, NULL);

		if (p_rsp_select->crc != crc_32)
		{
			pos_start = 0;
		}
	}
	else
	{
		pos_start = 0;
	}

	if (pos_start > 0 && pos_start < data_size)
	{
		len_remain = data_size - pos_start;
		err_code = dfu_serial_stream_data_crc(p_uart, p_data + pos_start, len_remain, pos_start, &crc_32);
		if (!err_code)
		{
			pos_start += len_remain;
		}
		else if (err_code == 2)
		{
			// when there is a CRC error, discard previous init packet
			err_code = 0;
			pos_start = 0;
		}
	}

	if (!err_code && pos_start == data_size)
	{
		err_code = dfu_serial_execute_obj(p_uart);
	}

	p_rsp_recover->offset = pos_start;

	return err_code;
}
*/

static int dfu_ble_execute_obj(sensorID_t *p_sm)
{
	int ret_code;
	uint8_t send_data[1];
	send_data[0] = NRF_DFU_OP_OBJECT_EXECUTE;

	//err_code = dfu_serial_send(p_uart, send_data, sizeof(send_data));
	ret_code = dfu_write_control_uuid(p_sm, send_data, 1);
	if(ret_code != GATTLIB_SUCCESS)
	{
		printf("Gattlib Error: %d\r\n", ret_code);
		return ret_code;
	}

	ret_code = dfu_ble_test_rsp(NRF_DFU_OP_OBJECT_SELECT);

	return ret_code;
}


int dfu_ble_send_init_packet(sensorID_t *p_sm, const uint8_t *p_data, uint32_t data_size)
{
	int err_code = 0;
	uint32_t crc_32 = 0;
	nrf_dfu_response_select_t rsp_select;
	nrf_dfu_response_select_t rsp_recover;

	printf("dfu_ble_send_init_packet()\r\n");

	if (p_data == NULL || !data_size)
	{
		printf("Invalid init packet!");

		err_code = 1;
	}

	if (!err_code)
	{
		err_code = dfu_ble_select_obj(p_sm, 0x01, &rsp_select);
	}
/*
	if (!err_code)
	{
		err_code = dfu_serial_try_to_recover_ip(p_uart, p_data, data_size, &rsp_recover, &rsp_select);

		if (!err_code && rsp_recover.offset == data_size)
			return err_code;
	}
*/
	if (!err_code)
	{
		if (data_size > rsp_select.max_size)
		{
			printf("Init packet too big!\r\n");

			err_code = 1;
		}
	}

	if (!err_code)
	{
		err_code = dfu_ble_create_obj(p_sm, 0x01, data_size);
	}

	if (!err_code)
	{
		err_code = dfu_ble_stream_data_crc(p_sm, p_data, data_size, 0, &crc_32);
	}
/*
	if (!err_code)
	{
		err_code = dfu_ble_execute_obj(p_sm);
	}
*/
	return err_code;
}


void Test_BLE_DFU(void)
{
	int ret_code;
	uint16_t *p_mtu;
	nrf_dfu_response_select_t p_select_rsp;
	
	sensorID_t sm = 
	{
		.p_name = "EVLIS",
		.p_addr = TEST_ADDR,
	};
	
	ret_code = dfu_ble_connect(&sm);
	if(ret_code != GATTLIB_SUCCESS)
	{
		goto EXIT;
	}
	
	ret_code = dfu_ble_set_prn(&sm, 0);
	//ret_code = dfu_ble_get_mtu(p_uart, &mtu); // not supported this SDK
	//ret_code = dfu_ble_ping(&sm, 77); // not supported this SDK
	//ret_code = dfu_ble_select_obj(&sm, 0x01, &p_select_rsp);
	//ret_code = dfu_ble_create_obj(&sm, 0x01, 7);

	//ret_code = dfu_ble_get_fw_ver(&sm, 1); // not supported this SDK
	//ret_code = dfu_ble_test_op(&sm);
	
EXIT:	
	dfu_ble_disconnect(&sm);
}
