BIN = test_dfu

CC = arm-linux-gnueabihf-gcc

CFLAGS = -g -O $(shell pkg-config --cflags glib-2.0) -Wall -O2 -I.
LIBS := -lm -lpthread $(shell pkg-config --libs glib-2.0) -lgattlib -lglib-2.0 -lpcre -lgio-2.0 -lgobject-2.0
LDFLAGS = -Xlinker --allow-shlib-undefined \
			-L/root/hub_dfu -Wl,-rpath,/root/hub_dfu /root/hub_dfu/libgattlib.so

DEPS = crc32.h \
       delay_connect.h \
       dfu.h \
       dfu_serial.h \
       logging.h \
       slip_enc.h \
       uart_drv.h \
       uart_slip.h \
       zip.h \
       miniz.h \
       jsmn.h \
	   dfu_transport_ble.h \
       Makefile
	   
OBJS = logging.o \
		delay_connect.o \
		crc32.o \
		dfu.o \
		dfu_transport_ble.o \
		jsmn.o \
		zip.o \
		main.o

%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -c $< -o $@

$(BIN): $(OBJS)
	$(CC) $(OBJS) $(LIBS) $(LDFLAGS) -o $(BIN)

clean: 
	rm -f $(BIN) $(OBJS)
